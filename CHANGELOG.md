# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> Infrastructure Services Deployment Package Using Helm ChangeLog

### 1.0.0 (2021-08-30)

Initial release of the on-premises environment

#### Features
* **Timescale** database
* **Neo4j** database
* **Kafka** message broker
* **Redis** distributed cache
* **ElasticSearch** and **Kibana** distributed logging
* **Prometheus** and **Grafana** distributed metrics
* **ApiSix** API gateway with ingress controller
* **KeyCloak** authentication and authorization integrated with **ApiSix**
* **connection_params** config map with connection parameters
* **connection_creds** secret with connection credentials

#### Breaking Changes
No breaking changes since this is the first version

#### Bug Fixes
No fixes in this version
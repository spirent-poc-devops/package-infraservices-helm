# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> Infrastructure Services Deployment Package Using Helm

Application deployment package for Infrastructure services that uses Helm charts. 
It shall be installed into any official VisionWorks environment: dev, test, onprem or cloud
prior to installing applications.

The deployment package installs and configures the following services into the "infra" namespace:
* **Timescale** database
* **Neo4j** database
* **Kafka** message broker
* **Redis** distributed cache
* **ElasticSearch** and **Kibana** distributed logging
* **Prometheus** and **Grafana** distributed metrics
* **ApiSix** API gateway with ingress controller
* **KeyCloak** authentication and authorization integrated with **ApiSix**

To communicate to applications connection parameters to the infrastructure services,
the Helm chart creates special objects in the "infra" namespace:
* **connection_params** config map with connection parameters
* **connection_creds** secret with connection credentials

<img src="design.png" width="800">

# Use

Before installing the application you shall provision an environment using one of the available provisioning scripts:
* [Development Environment](https://git.vwx.spirent.com/vwx-delivery/env-dev2-python)
* [Test Environment](https://git.vwx.spirent.com/vwx-delivery/env-test2-python)
* [On-Premises Environment](https://git.vwx.spirent.com/vwx-delivery/env-onprem2-python)
* [Cloud AWS Environment](https://git.vwx.spirent.com/vwx-delivery/env-cloudaws2-python)

To install the application
```bash
helm install infraservices . -n infra [--create-namespace]
```

To upgrade the application
```bash
helm upgrade infraservices . -n infra
```

To uninstall the application
```bash
help uninstall infraservices . -n infra
```

# Contacts

This environment was created and currently maintained by the team managed by *Sergey Seroukhov* and *Raminder Singh*.
